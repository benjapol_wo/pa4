package readability.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Debug tool-set for Readability.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.4.7 INTERNAL
 */
public class DebugTools {
	/** Enable the printing of debug messages to the console. */
	public static boolean consoleEnabled = false;
	private static State debugState = State.NONE;

	public static void setDebugState(int stateId) {
		switch (stateId) {
		case 0:
			debugState = State.NONE;
			break;
		case 1:
			debugState = State.ALL;
			break;
		}
	}

	public static void log(String log) {
		debugState.log(log);
	}

	public static void logln(String log) {
		debugState.log(String.format("%s\n", log));
	}

	public static void logln() {
		debugState.log(String.format("\n"));
	}
	
	public static void logf(String format, String log) {
		debugState.log(String.format(format, log));
	}

	private enum State {
		ALL {
			@Override
			public void log(String log) {
				if (consoleEnabled) {
					System.out.print(log);
				}
				logFilePrinter.print(log);
			}
		},
		NONE {
			@Override
			public void log(String log) {
				if (consoleEnabled) {
					System.out.print(log);
				}
			}
		};
		protected static PrintWriter logFilePrinter;
		static {
			try {
				logFilePrinter = new PrintWriter(new File("./ReadAbility_DebugLog.txt"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		protected abstract void log(String log);
	}
}
