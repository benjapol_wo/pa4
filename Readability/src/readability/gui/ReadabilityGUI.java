package readability.gui;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import readability.Readability;

/**
 * A Graphical User Interface (GUI) for Readability application.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.4.7
 */
public class ReadabilityGUI extends JFrame implements Runnable {

	/** Serial version UID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of the GUI.
	 */
	public ReadabilityGUI() {
		super("Readability by Benjapol Worakan");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Run the GUI.
	 */
	@Override
	public void run() {
		initComponent();
		pack();
		setVisible(true);
	}

	/**
	 * Initialize all GUI and other required components.
	 */
	private void initComponent() {
		Container contentPane = getContentPane();
		JPanel topPanel = new JPanel(), bottomPanel = new JPanel();
		JButton browseButton = new JButton("Browse"), countButton = new JButton(
				"Count"), clearButton = new JButton("Clear");
		JTextArea resultTextArea = new JTextArea(6, 80);
		JFileChooser filePicker = new JFileChooser();
		JTextField filePathTextField = new JTextField(50);
		
		resultTextArea.setLineWrap(true);
		resultTextArea.setEditable(false);

		topPanel.setLayout(new FlowLayout());
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.add(topPanel);
		contentPane.add(bottomPanel);

		topPanel.add(new JLabel("File location or URL: "));
		topPanel.add(filePathTextField);
		topPanel.add(browseButton);
		topPanel.add(countButton);
		topPanel.add(clearButton);

		bottomPanel.add(resultTextArea);
		
		filePicker.addChoosableFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return "Text files (*.txt)";
			}
			
			@Override
			public boolean accept(File f) {
				return f.isDirectory() && f.isFile() && f.getName().endsWith(".txt");
			}
			
		});
		
		browseButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int result = filePicker.showDialog(contentPane, "Pick file");
				if (result == JFileChooser.APPROVE_OPTION) {
					filePathTextField.setText(filePicker.getSelectedFile().getAbsolutePath());
				} else {
					filePathTextField.setText("Invalid file.");
				}
			}
			
		});
		
		countButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				resultTextArea.setText(Readability.getReadabilityInfo(filePathTextField.getText()));
			}
			
		});
		
		filePathTextField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				countButton.doClick();
			}
			
		});
		
		clearButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filePathTextField.setText("");
				resultTextArea.setText("");
			}
			
		});
		
		
	}
}
