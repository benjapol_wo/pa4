package readability;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

import readability.gui.ReadabilityGUI;
import readability.tools.DebugTools;

/**
 * Main application class. Decides whether to start the application in GUI mode
 * or CLI mode.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.4.12
 */
public class Readability {
	/**
	 * Entry point of the application, will process the arguments supplied in
	 * CLI mode. Will launch the application in GUI mode if there're no
	 * arguments.
	 * 
	 * @param args
	 *            - an array of String(s) which will be processed.
	 */
	public static void main(String[] args) {
		if (args.length > 0) {
			doProcess_CLI(args);
		} else {
			doProcess_GUI();
		}
	}

	/**
	 * Call the {@link EventQueue} to invoke {@link ReadabilityGUI}.
	 */
	private static void doProcess_GUI() {
		EventQueue.invokeLater(new ReadabilityGUI());
	}

	/**
	 * Start processing given arguments in Command-Line Interface (CLI) mode.
	 * 
	 * @param args
	 *            - arguments from {@link #main(String[])}
	 */
	private static void doProcess_CLI(String[] args) {
		System.out.println();
		for (String source : args) {
			System.out.println();
			System.out.println(getReadabilityInfo(source));
		}
	}

	/**
	 * Get readability information for a file or a URL.
	 * 
	 * @param fileNameOrURL
	 *            - a path to a file or a URL of a text source
	 * @return Formatted readability information of the source.
	 */
	public static String getReadabilityInfo(String fileNameOrURL) {
		String returnCache = "";

		try {
			int sentences = 0;

			if (fileNameOrURL.contains(":/") && !fileNameOrURL.contains(":\\")) {
				sentences = WordCounter.getInstance().countSentences(
						new URL(fileNameOrURL.trim()).openStream());
			} else {
				sentences = WordCounter.getInstance().countSentences(
						new FileInputStream(new File(fileNameOrURL)));
			}

			int words = WordCounter.getInstance().getWordCount();
			int syllables = WordCounter.getInstance().getSyllableCount();
			double readabilityIndex = WordCounter.getInstance()
					.getReadabilityIndex();
			String readabilityLevel = WordCounter.getInstance()
					.getReadabilityLevel();

			returnCache += ("Filename/URL: " + fileNameOrURL + "\n");
			returnCache += ("Number of Syllables: " + syllables + "\n");
			returnCache += ("Number of Words: " + words + "\n");
			returnCache += ("Number of Sentences: " + sentences + "\n");
			returnCache += ("Flesch Index: " + readabilityIndex + "\n");
			returnCache += ("Readability: " + readabilityLevel + "\n");

		} catch (Exception e) {
			if (e.getMessage() != null) {
				returnCache += "Error occurred when processing file: "
						+ fileNameOrURL + "\n" + e.getMessage() + "\n";
				DebugTools.logln(returnCache);
			}
		}
		return returnCache;
	}
}