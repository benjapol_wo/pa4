package readability;

import java.io.InputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

import readability.tools.DebugTools;

/**
 * A WordCounter that can count text sources for sentences, words, and
 * syllables. And later use them to calculate the Flesch's readability index and
 * readability level of the source.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.4.7
 */
public class WordCounter {
	/** A singleton instance of {@link WordCounter} */
	private static final WordCounter INSTANCE = new WordCounter();
	/**
	 * Current {@link State} of the WordCounter, for counting syllables in each
	 * word.
	 */
	private State state = State.INIT;
	/**
	 * Number of syllables from a word in the most recent
	 * {@link #countSyllables(String)} call.
	 */
	private int syllableCount;
	/**
	 * Number of syllables from the InputStream in the most recent
	 * {@link #countSentences(InputStream)} call.
	 */
	private int allSyllableCount;
	/**
	 * Number of sentences from the InputStream in the most recent
	 * {@link #countSentences(InputStream)} call.
	 */
	private int sentenceCount;
	/**
	 * Number of words from the InputStream in the most recent
	 * {@link #countSentences(InputStream)} call.
	 */
	private int wordCount;

	/**
	 * State(s) of the {@link WordCounter} when counting each character in a
	 * word for syllables. Needs to work with {@link WordCounter} to operate
	 * properly.
	 * 
	 * @author Benjapol Worakan 5710546577
	 * @version 15.4.7
	 */
	private enum State {
		/** Initial state of the {@link WordCounter}. */
		INIT {
			@Override
			public void handleChar(char c) {
				if (!Character.isLetter(c)) {
					getInstance().setState(END);
					getInstance().syllableCount = -1;
				} else if (c == 'E') {
					getInstance().setState(E_FIRST);
				} else if (c == 'A' || c == 'I' || c == 'O' || c == 'U'
						|| c == 'Y') {
					getInstance().setState(VOWEL);
				} else {
					getInstance().setState(CONSONANT);
				}
			}
		},
		/**
		 * A {@link State} that will be entered when {@link WordCounter} found a
		 * consonant character in a word while counting for syllables.
		 */
		CONSONANT {
			@Override
			public void handleChar(char c) {
				if (c == '-') {
					getInstance().setState(CONSONANT);
				} else if (!Character.isLetter(c)) {
					getInstance().setState(END);
					getInstance().syllableCount = -1;
				} else if (c == 'E') {
					getInstance().setState(E_FIRST);
				} else if (c == 'A' || c == 'I' || c == 'O' || c == 'U'
						|| c == 'Y') {
					getInstance().setState(VOWEL);
				} else {
					getInstance().setState(CONSONANT);
				}
			}
		},
		/**
		 * A {@link State} that will be entered when {@link WordCounter} found
		 * the first 'e' character after consonant(s) in a word, or after INIT
		 * state while counting for syllables.
		 */
		E_FIRST {
			@Override
			public void handleChar(char c) {
				if (c == '-') {
					getInstance().setState(CONSONANT);
				} else if (!Character.isLetter(c)) {
					getInstance().setState(END);
					getInstance().syllableCount = -1;
				} else if (c == 'A' || c == 'E' || c == 'I' || c == 'O'
						|| c == 'U') {
					getInstance().setState(VOWEL);
				} else {
					getInstance().countSyllable();
					getInstance().setState(CONSONANT);
				}
			}
		},
		/**
		 * A {@link State} that will be entered when {@link WordCounter} found a
		 * vowel character in a word while counting for syllables.
		 */
		VOWEL {
			@Override
			public void handleChar(char c) {
				if (c == '-') {
					getInstance().countSyllable();
					getInstance().setState(CONSONANT);
				} else if (!Character.isLetter(c)) {
					getInstance().setState(END);
					getInstance().syllableCount = 0;
				} else if (c == 'A' || c == 'E' || c == 'I' || c == 'O'
						|| c == 'U') {
					getInstance().setState(VOWEL);
				} else {
					getInstance().countSyllable();
					getInstance().setState(CONSONANT);
				}
			}
		},
		/**
		 * A {@link State} that will be entered when {@link WordCounter} reaches
		 * the end of the word while counting for syllables.
		 */
		END {
			@Override
			public void handleChar(char c) {
				if (!Character.isLetter(c)) {
					getInstance().syllableCount = 0;
				} else if (c == 'E' && getInstance().syllableCount == 0) {
					getInstance().countSyllable();
				} else if (c == 'A' || c == 'I' || c == 'O' || c == 'U'
						|| c == 'Y') {
					getInstance().countSyllable();
				}
			}
		};

		/**
		 * Handles each character from a word. This method will be called only
		 * by {@link WordCounter#handleChar(char)}.
		 * 
		 * @param c
		 *            - a character to be handled
		 */
		public abstract void handleChar(char c);
	}

	/**
	 * Get the singleton instance of {@link WordCounter}.
	 * 
	 * @return The singleton instance of {@link WordCounter}.
	 */
	public static WordCounter getInstance() {
		return INSTANCE;
	}

	/**
	 * A private empty constructor of {@link WordCounter}, to make it a
	 * singleton.
	 */
	private WordCounter() {

	}

	/**
	 * Handles each character from a word.
	 * 
	 * @param c
	 *            - a character to be handled
	 */
	private void handleChar(char c) {
		state.handleChar(c);
	}

	/**
	 * Change the {@link State} of the {@link WordCounter}.
	 * 
	 * @param state
	 *            - the {@link State} to be changed to
	 */
	private void setState(State state) {
		this.state = state;
	}

	/**
	 * Increment syllable count by one. Will be called by {@link State}.
	 */
	private void countSyllable() {
		syllableCount++;
	}

	/**
	 * Get number of sentences from an InputStream of a text source.
	 * 
	 * @param inputStream
	 *            - an InputStream that will be counted for sentences
	 * @return Number of sentences from the InputStream.
	 */
	public int countSentences(InputStream inputStream) {
		sentenceCount = 0;
		allSyllableCount = 0;
		wordCount = 0;
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(inputStream);
		scan.useDelimiter("\\n\\s*\\n|\\.\\s|\\!\\s|\\;\\s|\\?\\s|\\.\"|\\!\"|\\;\"|\\?\"|\\.\\)|\\!\\)|\\;\\)|\\?\\)");
		while (scan.hasNext()) {
			String sentence = scan.next();
			int wordsInSentence = countWords(sentence);
			DebugTools.logln("Sentence contains " + wordsInSentence
					+ " words. - " + sentence);
			if (wordsInSentence > 0) {
				sentenceCount++;
			}
		}
		return sentenceCount;
	}

	/**
	 * Get number of words in a String.
	 * 
	 * @param string
	 *            - a String that will be counted for words
	 * @return Number of words in a String.
	 */
	public int countWords(String string) {
		int wordCount = 0;
		string = convertPunctuationToSpace(string);
		StringTokenizer sentenceToken = new StringTokenizer(string);
		while (sentenceToken.hasMoreTokens()) {
			int syllablesInWord = countSyllables(sentenceToken.nextToken());
			allSyllableCount += syllablesInWord;
			if (syllablesInWord > 0) {
				wordCount++;
				this.wordCount++;
			}
		}
		return wordCount;
	}

	/**
	 * Get number of syllables from the given word.
	 * 
	 * @param word
	 *            - a word to count for syllables
	 * @return Number of syllables in given word.
	 */
	public int countSyllables(String word) {
		syllableCount = 0;
		state = State.INIT;

		DebugTools.logln("Word: " + word);

		for (char c : word.toCharArray()) {
			DebugTools.log(" Char: " + c + "  | Ins: "
					+ String.format("%-12s", state + "/" + syllableCount)
					+ " | Outs: ");
			if (c == '\'') {
				DebugTools.log("_SKIPPING APOSTROPHE");
				continue;
			}
			handleChar(Character.toUpperCase(c));

			DebugTools.logln(state + "/" + syllableCount);
			if (state == State.END) {
				break;
			}
		}

		DebugTools.log(" TERMINATE| Ins: "
				+ String.format("%-12s", state + "/" + syllableCount)
				+ " | Outs: ");
		state = State.END;
		handleChar(Character.toUpperCase(word.charAt(word.length() - 1)));
		DebugTools.logln(state + "/" + syllableCount);
		if (syllableCount < 0) {
			syllableCount = 0;
		}

		DebugTools.logln();

		return syllableCount;
	}

	/**
	 * Convert any defined punctuation marks in a String to whitespace, for
	 * private use inside {@link #countWords(String)} and
	 * {@link #countSyllables(String)} methods.
	 * 
	 * @param string
	 *            - a String to be converted
	 * @return The converted String.
	 */
	private String convertPunctuationToSpace(String string) {
		DebugTools
				.logln("LineText conversion for syllables counter\n OriginalLine:  "
						+ string);
		String convertedString = string
				.replaceAll("---", " ")
				.replaceAll("--", " ")
				.replaceAll(
						"\\,|\\.|\\;|\\!|\\?|\\(|\\)|\\[|\\]|\\/|\"|\\:|\\{|\\}|\\*|\\&",
						" ");
		DebugTools.logln(" ConvertedLine: " + convertedString);
		DebugTools.logln();
		return convertedString;
	}

	/**
	 * Get number of all syllables from the InputStream that was recently given
	 * to {@link #countSentences(InputStream)}.
	 * 
	 * @return Number of syllables in from the InputStream that was recently
	 *         given to {@link #countSentences(InputStream)}.
	 */
	public int getSyllableCount() {
		return allSyllableCount;
	}

	/**
	 * Get number of words from the InputStream in the most recent
	 * {@link #countSentences(InputStream)} call.
	 * 
	 * @return Number of words from the InputStream in the most recent
	 *         {@link #countSentences(InputStream)} call.
	 */
	public int getWordCount() {
		return wordCount;
	}

	/**
	 * Get number of sentences from the InputStream in the most recent
	 * {@link #countSentences(InputStream)} call.
	 * 
	 * @return Number of sentences from the InputStream in the most recent
	 *         {@link #countSentences(InputStream)} call.
	 */
	public int getSentenceCount() {
		return sentenceCount;
	}

	/**
	 * Get the Flesch's readability index of the text in the InputStream that
	 * was recently given to {@link #countSentences(InputStream)}. The
	 * readability index will be calculated using the Flesch's formula.
	 * 
	 * @return The Flesch's readability index of the text in the InputStream
	 *         that was recently given to {@link #countSentences(InputStream)}.
	 */
	public double getReadabilityIndex() {
		return 206.835 - 84.6
				* ((double) allSyllableCount / (double) wordCount) - 1.015
				* ((double) wordCount / (double) sentenceCount);
	}

	/**
	 * Get the readability level of the text in the InputStream that was
	 * recently given to {@link #countSentences(InputStream)}. The readability
	 * level will be calculated from Flesch's readability index.
	 * 
	 * @return The readability level of the text in the InputStream that was
	 *         recently given to {@link #countSentences(InputStream)}.
	 */
	public String getReadabilityLevel() {
		if (getReadabilityIndex() > 100) {
			return "4th grade student (elementary school)";
		} else if (getReadabilityIndex() > 90) {
			return "5th grade student";
		} else if (getReadabilityIndex() > 80) {
			return "6th grade student";
		} else if (getReadabilityIndex() > 70) {
			return "7th grade student";
		} else if (getReadabilityIndex() > 65) {
			return "8th grade student";
		} else if (getReadabilityIndex() > 60) {
			return "9th grade student";
		} else if (getReadabilityIndex() > 50) {
			return "High school student";
		} else if (getReadabilityIndex() > 30) {
			return "College student";
		} else if (getReadabilityIndex() >= 0) {
			return "College graduate";
		} else {
			return "Advanced degree student";
		}
	}
}